<?php require "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
</head>
<body>

    <?php createProduct("Hotdog", 100); ?>
    <?php createProduct("Burger", 100); ?>

    <h4>Products</h4>
	<ul>
        <?php printGrades($products); ?>
    </ul>

    <h4>Products Length</h4>
    <span>
    	<?php countProducts(); ?>
    </span>

    <?php deleteProduct(); ?>

    <h4>Remaining Products after delete</h4>
    <ul>
        <?php printGrades($products); ?>
    </ul>

</body>
</html>